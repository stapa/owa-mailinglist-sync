import json
from datetime import datetime
from urllib.parse import quote


def ask_confirm():
    answer = ""
    while answer not in ["y", "n"]:
        answer = input("Do you want to proceed [Y/N]? ").lower()
    return answer == "y"


def timestamp():
    return int(round(datetime.now().timestamp()))


def timestamp_iso_utc_ms():
    return datetime.utcnow().isoformat(timespec='milliseconds')


def make_client_string(client):
    client_string = client.cookies['ClientId']
    client_string += "_"
    client_string += str(round(datetime.now().timestamp() * 100000))
    return client_string


def make_directory_search_request(qs):
    header = {
        "__type": "JsonRequestHeaders:#Exchange",
        "RequestServerVersion": "Exchange2013",
        "TimeZoneContext":
            {
                "__type": "TimeZoneContext:#Exchange",
                "TimeZoneDefinition":
                    {
                        "__type": "TimeZoneDefinitionType:#Exchange",
                        "Id": "W. Europe Standard Time"
                    }
            }
    }
    body = {
        "__type": "FindPeopleRequest:#Exchange",
        "IndexedPageItemView":
            {
                "__type": "IndexedPageView:#Exchange",
                "BasePoint": "Beginning",
                "Offset": 0,
                "MaxEntriesReturned": 100
            },
        "QueryString": qs,
        "ParentFolderId":
            {
                "__type": "TargetFolderId:#Exchange",
                "BaseFolderId":
                    {
                        "__type": "DistinguishedFolderId:#Exchange",
                        "Id": "directory"
                    }
            },
        "PersonaShape":
            {
                "__type": "PersonaResponseShape:#Exchange",
                "BaseShape": "Default",
                "AdditionalProperties":
                    [
                        {
                            "__type": "PropertyUri:#Exchange",
                            "FieldURI": "PersonaAttributions"
                        }
                    ]
            },
        "ShouldResolveOneOffEmailAddress": False,
        "SearchPeopleSuggestionIndex": False,
        "Context":
            [
                {
                    "__type": "ContextProperty:#Exchange",
                    "Key": "AppName",
                    "Value": "OWA"
                },
                {
                    "__type": "ContextProperty:#Exchange",
                    "Key": "AppScenario",
                    "Value": ""
                },
                {
                    "__type": "ContextProperty:#Exchange",
                    "Key": "ClientSessionId",
                    "Value": ""
                }
            ]
    }
    request = {
        "__type": "FindPeopleJsonRequest:#Exchange",
        "Header": header,
        "Body": body
    }

    quoted_request = quote(json.dumps(request, separators=(",", ":")))
    return quoted_request
