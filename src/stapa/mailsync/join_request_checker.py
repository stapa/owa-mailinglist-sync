import re
from email.parser import BytesParser
from email.policy import default
from imaplib import IMAP4_SSL

import pytz
import pandas as pd
from tqdm.auto import tqdm


def fetch_mail(M, num):
    typ, data = M.fetch(num, '(RFC822)')
    res = {}
    if typ != "OK":
        res['error'] = True
        res['reason'] = "FETCH_ERROR-" + typ
        return res

    email = BytesParser(policy=default).parsebytes(data[0][1])
    subject = email['Subject']
    first_rcv = email.get_all("Received")[-1]
    res['error'] = False
    res['from'] = email['From'].addresses[0].addr_spec
    res['subject'] = str(subject)
    res['date'] = str(email['Date'])

    if not re.findall(r"^by \w+\.hosting\.ovh\.net \(", first_rcv):
        res["state"] = 'NOT_WEBSITE_EMAIL'
        return res

    res['state'] = "WEBSITE_EMAIL"

    payload = email.get_payload(decode=True).decode()
    to_add = re.findall("De : [^<]+<([^>]+)>\r\n.+", payload)
    if not to_add:
        res['error'] = True
        res['reason'] = "PARSE_ERROR"
        return

    res['address'] = to_add[0]
    res['domain'] = to_add[0].split('@', 1)[1]
    return res


def fetch_join_requests(username, password, start_ts):
    M = IMAP4_SSL("email.pasteur.fr")
    M.login(username + "@pasteur.fr/stapa", password)
    typ, data = M.select('"Add to Mailing List"')
    typ, data = M.search(None, "ALL")

    res = pd.DataFrame([fetch_mail(M, num) for num in tqdm(data[0].split())])
    res['date'] = pd.to_datetime(res['date'],
                                 format="%a, %d %b %Y %H:%M:%S %z")
    start_datetime = pd.to_datetime(start_ts, unit='s').tz_localize(pytz.utc)
    to_add = res[(res.state == "WEBSITE_EMAIL")
                 & (res.domain == "pasteur.fr")
                 & (res.date > start_datetime)]

    if not to_add.empty:
        last_datetime = to_add.sort_values('date').iloc[-1]['date']
        last_ts = int(last_datetime.tz_convert(pytz.utc).timestamp()) + 1
    else:
        last_ts = start_ts
    return to_add, last_ts
