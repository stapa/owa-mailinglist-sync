import pkgutil
from smtplib import SMTP
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from time import sleep
from tqdm.auto import tqdm


def get_resource(name):
    return pkgutil.get_data(__package__, 'resources/' + name)


class WelcomeMailSender:
    def __init__(self, username, password):
        self.username = username
        self.password = password

        text = get_resource('welcome_mail.txt').decode('utf-8')
        html = get_resource('welcome_mail.html').decode('utf-8')

        text_part = MIMEText(text, 'plain')
        html_part = MIMEText(html, 'html')

        self._msg_alternative = MIMEMultipart('alternative')
        self._msg_alternative.attach(text_part)
        self._msg_alternative.attach(html_part)

        filename = 'StaPaMailingList.pdf'
        attachment = get_resource(filename)
        self._msg_attachment = MIMEApplication(attachment, _subtype="pdf")
        self._msg_attachment.add_header('Content-Disposition', 'attachment',
                                        filename=filename)

    def prepare_msg(self, to):
        msg_mixed = MIMEMultipart('mixed')
        msg_mixed.attach(self._msg_alternative)
        msg_mixed.attach(self._msg_attachment)
        msg_mixed['From'] = 'StaPa <stapa@pasteur.fr>'
        msg_mixed['To'] = to
        msg_mixed['Subject'] = '🎉 Welcome to the StaPa mailing list!'
        return msg_mixed

    def send_mails(self, emails):
        s = SMTP(host='email.pasteur.fr', port=587)
        s.starttls()
        s.login(self.username + "@pasteur.fr", self.password)

        for email in tqdm(emails):
            msg = self.prepare_msg(email)
            s.sendmail(msg['From'], msg['To'], msg.as_string())
            sleep(1)
