import json
from getpass import getpass
from urllib.parse import urlencode, urljoin, quote
from uuid import uuid4

import pandas as pd

from .utils import (make_client_string,
                    make_directory_search_request,
                    timestamp_iso_utc_ms,
                    timestamp)


class MailingListManager:
    BASE = "https://email.pasteur.fr"

    def __init__(self, sso_client):
        self.client = sso_client
        self.action_counter = 0

    def managed_lists(self):
        # Required to obtain a msExchEcp canary
        self.client.get(urljoin(self.BASE,
                        "/ecp/MyGroups/PersonalGroups.aspx?showhelp=false"))

        body = {'filter': {'SearchText': None},
                'sort': {'Direction': 0, 'PropertyName': "DisplayName"}}
        headers = {'Origin': self.BASE,
                   'X-Requested-With': "XMLHttpRequest",
                   'Content-Type': "application/json; charset=utf-8"}
        query = {'ActivityCorrelationID': str(uuid4()),
                 'reqId': timestamp(),
                 'msExchEcpCanary': self.client.cookies['msExchEcpCanary']}
        url = urljoin(self.BASE,
                      ("/ecp/MyGroups/OwnedGroups.svc/GetList?"
                       + urlencode(query)))
        res = self.client.post(url, headers=headers, json=body)
        return pd.json_normalize(res.json()['d']['Output'])

    def __getitem__(self, address):
        lists = self.managed_lists()
        lists = lists[lists['PrimarySmtpAddress'] == address]
        if len(lists) != 1:
            raise Exception("List not found or too many matches.")
        return MailingList(self, lists.iloc[0])

    def find_people(self, querystring):
        self.action_counter = self.action_counter - 1
        headers = {
            'Origin': "https://email.pasteur.fr",
            'Action': "FindPeople",
            'Content-Type': "application/json; charset=utf-8",
            'client-request-id': make_client_string(self.client),
            'X-OWA-ActionId': str(self.action_counter),
            'X-OWA-ActionName': "PeopleModule",
            'X-OWA-Attempt': "1",
            'X-OWA-CANARY': self.client.cookies['X-OWA-CANARY'],
            'X-OWA-ClientBegin': timestamp_iso_utc_ms(),
            'X-OWA-ClientBuildVersion': "15.2.922.7",
            'X-OWA-CorrelationId': make_client_string(self.client),
            'X-OWA-UrlPostData': make_directory_search_request(querystring),
            'X-Requested-With': "XMLHttpRequest"
        }
        query = {
            'action': "FindPeople",
            'EP': 1,
            'ID': self.action_counter,
            'AC': 1
        }
        url = "https://email.pasteur.fr/owa/service.svc?" + urlencode(query)
        res = self.client.post(url, headers=headers)
        return pd.json_normalize(res.json()['Body']['ResultSet'])


class MailingList:
    BASE = "https://email.pasteur.fr"

    def __init__(self, manager, mailing_list):
        self.manager = manager
        self.client = manager.client
        self.mailing_list = mailing_list

    def members(self):
        body = {
            'identity': {
                k.split('Identity.')[1]: v
                for k, v in self.mailing_list.to_dict().items()
                if k.startswith('Identity.')
            }
        }
        headers = {
            'Origin': self.BASE,
            'X-Requested-With': "XMLHttpRequest",
            'Content-Type': "application/json; charset=utf-8"
        }
        query = {
            'ActivityCorrelationID': str(uuid4()),
            'reqId': timestamp(),
            'msExchEcpCanary': self.client.cookies['msExchEcpCanary']
        }
        url = urljoin(self.BASE, ("/ecp/MyGroups/OwnedGroups.svc/GetObject?"
                                  + urlencode(query)))
        res = self.client.post(url, headers=headers, json=body)
        return pd.json_normalize(res.json()['d']['Output'][0]['Members'])

    def _address_lookup(self, address, members):
        output = {}
        results = self.manager.find_people(address)
        if len(results) == 0:
            output['error'] = True
            output['state'] = "NO_RESULTS"
        elif len(results) > 1:
            output['error'] = True
            output['state'] = "AMBIGUOUS"
        else:
            output['error'] = False
            result = results.iloc[0]
            result_mail = result['EmailAddress.EmailAddress']
            if result_mail in members['PrimarySmtpAddress'].values:
                output['state'] = "ALREADY_SUBSCRIBED"
            else:
                output['state'] = "NEW"
                output['identity'] = {
                    '__type': "Identity:ECP",
                    'DisplayName': " ".join(
                        [result['GivenName'], result['Surname']]),
                    'RawIdentity': " ".join(
                        [result['EmailAddress.EmailAddress']])
                }
        return output

    def bulk_check(self, address_list, members=None):
        if members is None:
            members = self.members()

        results = {
            address: self._address_lookup(address, members)
            for address in address_list
        }

        return results

    def bulk_add(self, address_list):
        members = self.members()

        member_identities = [
            {
                k.split('Identity.')[1]: v
                for k, v in member[1].to_dict().items()
                if k.startswith('Identity.')
            }
            for member in members.iterrows()
        ]

        results = self.bulk_check(address_list, members)

        for address, result in results.items():
            if result['state'] == "NEW":
                member_identities.append(result['identity'])
                result['state'] = "ADDED"

        body = {
            'identity': {
                k.split('Identity.')[1]: v
                for k, v in self.mailing_list.to_dict().items()
                if k.startswith('Identity.')
            },
            'properties': {
                'Members': member_identities
            }
        }
        headers = {
            'Origin': self.BASE,
            'X-Requested-With': "XMLHttpRequest",
            'Content-Type': "application/json; charset=utf-8"
        }
        query = {
            'ActivityCorrelationID': str(uuid4()),
            'reqId': timestamp(),
            'msExchEcpCanary': self.client.cookies['msExchEcpCanary']
        }
        url = urljoin(self.BASE, ("/ecp/MyGroups/OwnedGroups.svc/SetObject?"
                                  + urlencode(query)))
        res = self.client.post(url, headers=headers, json=body)

        err = res.json()['d']['ErrorRecords']
        if err:
            raise Exception(f"Add failed: {err}!")
        return {k: v['state'] for k, v in results.items()}
