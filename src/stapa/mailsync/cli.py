import argparse
import json
import os
from datetime import datetime
from getpass import getpass
from pathlib import Path

import pandas as pd
import xdgappdirs
from zolfa.zauth.pasteur import PasteurEmail

from .join_request_checker import fetch_join_requests
from .mailinglist import MailingListManager
from .utils import ask_confirm
from .welcome_mail import WelcomeMailSender

data_dir = Path(xdgappdirs.user_data_dir("stapa_mailsync"))
members_backup_dir = data_dir / "backup_members"
members_backup_dir.mkdir(parents=True, exist_ok=True)


def sync():
    parser = argparse.ArgumentParser()
    parser.add_argument('username', action="store",
                        help='PasteurID username')
    parser.add_argument('start_ts', action="store",
                        help='First timestamp to take into account')
    parser.add_argument('-y', action="store_true",
                        help='Run non-interactively, confirm automatically,'
                             ' password must be provided as PASTEURID_PASSWORD'
                             ' env variable.')
    parser.add_argument('--membersfile', help='output csv members file')
    parser.add_argument('--timestampfile', help='output timestamp file')
    parser.add_argument('--statsfile', help='output stats file')
    args = parser.parse_args()

    print(f'DATA_DIR: {data_dir}')
    print(f'CUR_DIR: {os.getcwd()}')

    if args.y:
        password = os.environ.get('PASTEURID_PASSWORD', '')
        if not password:
            print('Error, running non-interactively (-y) but password not'
                  ' available in env variable PASTEURID_PASSWORD')
            return -1
    else:
        password = getpass('PasteurID password: ')

    if args.start_ts == 'auto':
        with (data_dir / 'last_timestamp.txt').open() as f:
            start_ts = f.read()
            print(f"Initial timestamp read: {start_ts}")
    else:
        start_ts = args.start_ts

    print("Checking new join requests...")
    to_add, last_ts = fetch_join_requests(args.username,
                                          password,
                                          start_ts)

    sso = PasteurEmail(args.username, password)
    sso.authenticate()
    mm = MailingListManager(sso.client)
    ml = mm['stapa-all@pasteur.fr']
    ts = datetime.utcnow().isoformat(timespec='seconds')
    members = ml.members()
    members.to_csv(members_backup_dir / f'{ts}.csv')

    print("Verifying requests...")

    to_add = to_add.drop_duplicates(subset='address', keep='last')
    to_add = to_add.set_index('address')
    to_check = set(to_add.index)

    if len(to_check) == 0:
        print('No new requests.')
        return

    check_results = ml.bulk_check(to_check)
    check_results = pd.DataFrame([
        {'address': k,
         'display_name': (v['identity'].get('DisplayName')
                          if 'identity' in v else ''),
         'request_date': to_add.loc[k].date,
         'error': v['error'],
         'state': v['state']
         }
        for k, v in check_results.items()
    ])
    print('\nRequests:\n')

    print(check_results.to_markdown())

    to_add = check_results[check_results.state == 'NEW']
    print(f"\n{len(to_add)} new members will be added to 'stapa-all'.")

    if len(to_add) == 0:
        return

    if not args.y:
        if not ask_confirm():
            return

    add_results = ml.bulk_add(set(to_add['address']))
    ts = datetime.utcnow().isoformat(timespec='seconds')
    log_entry = {ts: add_results}

    with (data_dir / 'log.txt').open('a+') as f:
        f.write(json.dumps(log_entry) + '\n')
    with (data_dir / 'last_timestamp.txt').open('w') as f:
        f.write(str(last_ts))

    check_results = check_results.set_index('address')
    for k, v in add_results.items():
        check_results.loc[k, 'state'] = v
    check_results = check_results.reset_index()
    print('\nResults:\n')
    print(check_results.to_markdown())

    added_addresses = check_results[check_results.state == 'ADDED'].address
    added_addresses = added_addresses.to_list()
    print('\nSending welcome mail to:')
    print("; ".join(added_addresses))
    print("")

    ts = datetime.utcnow().isoformat(timespec='seconds')
    members = ml.members()
    members.to_csv(members_backup_dir / f'{ts}.csv')

    if args.membersfile:
        members[[
            'DisplayName',
            'Identity.DisplayName',
            'PrimarySmtpAddress',
            'RecipientTypeDetailsString',
            'Identity.RawIdentity']
        ].to_csv(args.membersfile, index=False)
    if args.statsfile:
        key = 'RecipientTypeDetailsString'
        member_count = members.groupby(key)[key].count()
        with open(args.statsfile, 'w') as f:
            json.dump({
                'active_users': int(member_count['UserMailbox']),
                'disabled_users': int(member_count['DisabledUser']),
                'total': len(members)
            }, f, indent=4)
    if args.timestampfile:
        with open(args.timestampfile, 'w') as f:
            f.write(str(last_ts))

    wms = WelcomeMailSender(args.username, password)
    wms.send_mails(added_addresses)

    print("Welcome emails sent!")


if __name__ == '__main__':
    sync()
